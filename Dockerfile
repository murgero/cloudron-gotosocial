FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/pkg /app/code
WORKDIR /app/code

RUN apt-get update && \
    apt-get -y install graphicsmagick recutils jq && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN pip3 install yq

COPY start.sh /app/pkg/
COPY config.yaml /app/pkg/
ADD ./gotosocial/web /app/pkg/web
COPY gotosocial/gotosocial /app/code/

RUN chmod +x /app/code/gotosocial

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log


CMD [ "/app/pkg/start.sh" ]
