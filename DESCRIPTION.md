# About

ActivityPub Server compatible with Mastodon, Pleroma, and more!

## Make Admin Account

Change the `username` variable to whatever you want, and change `password` as well.

`/app/code/gotosocial --account-domain $CLOUDRON_APP_DOMAIN --host $CLOUDRON_APP_DOMAIN --config-path /app/data/config.yaml admin account create --username socialadmin --email username@example.com --password 'password'`

Then confirm:

`/app/code/gotosocial --account-domain $CLOUDRON_APP_DOMAIN --host $CLOUDRON_APP_DOMAIN --config-path /app/data/config.yaml admin account confirm --username socialadmin`

Then Promote:

`/app/code/gotosocial --account-domain $CLOUDRON_APP_DOMAIN --host $CLOUDRON_APP_DOMAIN --config-path /app/data/config.yaml admin account promote --username socialadmin`

## Make normal account

Change the `username` variable to whatever you want, and change `password` as well.

`/app/code/gotosocial --account-domain $CLOUDRON_APP_DOMAIN --host $CLOUDRON_APP_DOMAIN --config-path /app/data/config.yaml admin account create --username socialadmin --email username@example.com --password 'password'`

Then confirm:

`/app/code/gotosocial --account-domain $CLOUDRON_APP_DOMAIN --host $CLOUDRON_APP_DOMAIN --config-path /app/data/config.yaml admin account confirm --username socialadmin`
