[0.1.0]
* Initial version

[0.2.0]
* Update description

[0.3.0]
* Use latest version 
* Fix Firefox
* Allow custom configuration

[1.0.0]
* Update to latest version 0.127.0
* add stable testing for future updates
* change manifest information

[1.0.1]
* Update n8n to 0.130.0

[1.1.0]
* Update n8n to 0.131.0

[1.1.1]
* Fix manual executing loading forever

[1.2.0]
* Update n8n to 0.132.1
* remove supervisord for nginx and n8n
* run n8n directly

[1.3.0]
* Update n8n to 0.132.2
* disable update notifications
* run as normal cloudron user

[1.4.0]
* Update n8n to 0.133.0
* Remove sendmail addon, please create a mailbox in Cloudro or outside Cloudron and use those credentials instead.

[1.4.1]
* Update n8n to 0.134.0

[1.5.0]
* Update n8n to 0.135.1

[1.5.1]
* Update n8n to 0.135.2

[1.6.0]
* Update n8n to 0.136.0

[1.7.0]
* Update n8n to 0.137.0

[1.8.0]
* Update n8n to 0.138.0

[1.9.0]
* Update n8n to 0.139.0

[1.10.0]
* Update n8n to 0.140.0

[1.11.0]
* Update n8n to 0.141.0

[1.11.1]
* Update n8n to 0.141.1

[1.12.0]
* Update n8n to 0.142.0

[1.13.0]
* Update n8n to 0.143.0

[1.14.0]
* Update n8n to 0.144.0

[1.15.0]
* Update n8n to 0.145.0

[1.16.0]
* Update n8n to 0.146.0
* Re-work data directory layout to be more clear
* Support for using [custom node modules](https://docs.cloudron.io/apps/n8n/#custom-node-modules)

[1.17.0]
* Update n8n to 0.147.1

[1.18.0]
* Update n8n to 0.148.0

[1.19.0]
* Update n8n to 0.149.0

[1.20.0]
* Update n8n to 0.150.0

[1.21.0]
* Update n8n to 0.151.0

[1.22.0]
* Update n8n to 0.152.0

[1.23.0]
* Update n8n to 0.153.0

[1.24.0]
* Update n8n to 0.155.1

[1.24.1]
* Update n8n to 0.155.2

[1.25.0]
* Update n8n to 0.156.0

[1.25.1]
* Update n8n to 0.157.0

[1.25.2]
* Update n8n to 0.157.0

[1.25.3]
* Update n8n to 0.157.1

[1.26.0]
* Update n8n to 0.158.0

[1.26.1]
* Add `marked` npm module

[1.26.2]
* Update n8n to 0.159.0

[1.26.3]
* Update n8n to 0.159.1

[1.27.0]
* Update n8n to 0.161.0

[1.27.1]
* Update n8n to 0.161.1

[1.28.0]
* Update n8n to 0.162.0

[1.28.1]
* Update n8n to 0.163.1

[1.29.0]
* Update n8n to 0.164.1

[1.30.0]
* Update n8n to 0.165.1

[1.31.0]
* Update n8n to 0.166.0

[1.32.0]
* Add pandoc and asciidoctor

[1.32.1]
* Update n8n to 0.167.0

[2.0.0]
* Update n8n to 0.168.2
* Cloudron proxyAuth is removed, since n8n now has its own usermanagement. Make sure to create local users within n8n from now on to keep workflows private.

[2.0.1]
* Integrate with Cloudron's email sending addon

[2.1.0]
* Update n8n to 0.169.0

[2.2.0]
* Update n8n to 0.170.0

[2.2.1]
* Update n8n to 0.171.0

[2.2.2]
* Update n8n to 0.171.1

[2.2.3]
* Update n8n to 0.172.0

[2.2.4]
* Update n8n to 0.173.0

[2.2.5]
* Update n8n to 0.173.1

[2.3.0]
* Update n8n to 0.174.0

[2.3.1]
* Update n8n to 0.175.0

[2.3.2]
* Update n8n to 0.175.1

[2.3.3]
* Update n8n to 0.176.0

[2.4.0]
* Update n8n to 0.177.0

[2.4.1]
* Update n8n to 0.178.0

[2.4.2]
* Update n8n to 0.178.1

[2.4.3]
* Update n8n to 0.178.2

[2.4.4]
* Update n8n to 0.179.0

[2.4.5]
* Update n8n to 0.180.0

[2.5.0]
* Update n8n to 0.181.0
* Enable easier CLI use

[2.5.1]
* Update n8n to 0.181.1

[2.6.0]
* Update n8n to 0.181.2

[2.7.0]
* Update n8n to 0.182.1

[2.8.0]
* Update n8n to 0.183.0

[2.8.1]
* Update n8n to 0.184.0

[2.8.2]
* Update n8n to 0.185.0

[2.8.3]
* Update n8n to 0.186.0

[2.8.4]
* Update n8n to 0.186.1

[2.8.5]
* Update n8n to 0.167.1

[2.8.6]
* Update n8n to 0.167.2

[2.8.7]
* Update n8n to 0.188.0

[2.8.8]
* Update n8n to 0.189.0

[2.8.9]
* Update n8n to 0.189.1

[2.8.10]
* Update n8n to 0.190.0

[2.8.11]
* Update n8n to 0.191.0

[2.8.12]
* Update n8n to 0.191.1

[2.8.13]
* Update n8n to 0.192.0

[2.8.14]
* Update n8n to 0.192.1

[2.8.15]
* Update n8n to 0.192.2

[2.8.16]
* Update n8n to 0.193.1

[2.8.17]
* Update n8n to 0.193.2

[2.8.18]
* Update n8n to 0.193.3

[2.8.19]
* Update n8n to 0.193.4

[2.8.20]
* Update n8n to 0.193.5

[2.8.21]
* Update n8n to 0.194.0

[2.8.22]
* Update n8n to 0.195.3

[2.8.23]
* Update n8n to 0.195.4

[2.8.24]
* Update n8n to 0.195.5

[2.9.0]
* Update n8n to 0.197.1

[2.10.0]
* Update n8n to 0.198.0

[2.11.0]
* Update n8n to 0.198.2

[2.12.0]
* Update n8n to 0.199.0

[2.13.0]
* Update n8n to 0.200.0

[2.13.1]
* Update n8n to 0.200.1

[2.14.0]
* Update n8n to 0.201.0

[2.14.1]
* Fix formatting of display name

[2.15.0]
* Update n8n to 0.202.1

[2.16.0]
* Update n8n to 0.203.0

[2.16.1]
* Update n8n to 0.203.1

[2.16.2]
* Fix installation of community modules
* Add ajv-formats module

[2.17.0]
* Update n8n to 0.204.0

[2.18.0]
* Update n8n to 0.205.0

[2.19.0]
* Update n8n to 0.206.1

[2.20.0]
* Update n8n to 0.207.0

[2.20.1]
* Update n8n to 0.207.1

[2.21.0]
* Update n8n to 0.208.0

[2.21.1]
* Update n8n to 0.208.1

