#!/bin/bash

set -eu

echo "=> Ensure directories"
mkdir -p /run/public

if [ ! -f /app/data/config.yaml ]; then
    cp /app/pkg/config.yaml /app/data/config.yaml
    cp /app/pkg/web -r /app/data/web
    ## Since config is new, let's overwrite *some* stuff.
    ## DB Stuff
    /usr/local/bin/yq -Y ".\"db-type\" = \"postgres\"" /app/data/config.yaml -i
    /usr/local/bin/yq -Y ".\"db-address\" = \"$CLOUDRON_POSTGRESQL_HOST\"" /app/data/config.yaml -i
    /usr/local/bin/yq -Y ".\"db-port\" = \"$CLOUDRON_POSTGRESQL_PORT\"" /app/data/config.yaml -i
    /usr/local/bin/yq -Y ".\"db-user\" = \"$CLOUDRON_POSTGRESQL_USERNAME\"" /app/data/config.yaml -i
    /usr/local/bin/yq -Y ".\"db-password\" = \"$CLOUDRON_POSTGRESQL_PASSWORD\"" /app/data/config.yaml -i
    /usr/local/bin/yq -Y ".\"db-database\" = \"$CLOUDRON_POSTGRESQL_DATABASE\"" /app/data/config.yaml -i
    ## Email Config
    /usr/local/bin/yq -Y ".\"smtp-host\" = \"$CLOUDRON_POSTGRESQL_DATABASE\"" /app/data/config.yaml -i
    /usr/local/bin/yq -Y ".\"smtp-port\" = \"$CLOUDRON_MAIL_SMTPS_PORT\"" /app/data/config.yaml -i
    /usr/local/bin/yq -Y ".\"smtp-username\" = \"$CLOUDRON_MAIL_SMTP_USERNAME\"" /app/data/config.yaml -i
    /usr/local/bin/yq -Y ".\"smtp-password\" = \"$CLOUDRON_MAIL_SMTP_PASSWORD\"" /app/data/config.yaml -i
    /usr/local/bin/yq -Y ".\"smtp-from\" = \"$CLOUDRON_MAIL_FROM\"" /app/data/config.yaml -i
    
fi

echo "=> Setting permissions"
chown -R cloudron:cloudron /app/data /run


echo "==> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i gotosocial

# echo "=> Starting GTS"
# cd /app/data/
# exec gosu cloudron:cloudron /app/code/gotosocial --account-domain $CLOUDRON_APP_DOMAIN --host $CLOUDRON_APP_DOMAIN --config-path /app/data/config.yaml server start
